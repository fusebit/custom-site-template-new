# VVV Custom site template
For when you just need a simple dev site

## Overview
This template will allow you to create a basic dev environment using only `vvv-custom.yml`.

The supported environments are:
- A single site

# Configuration

### The minimum required configuration:

```
my-site:
  repo: https://bitbucket.org/fusebit/custom-site-template-new
  hosts:
    - my-site.test
```
| Setting    | Value       |
|------------|-------------|
| Domain     | my-site.test |
| Site Title | my-site.test |
| DB Name    | my-site     |
| Site Type  | Single      |

